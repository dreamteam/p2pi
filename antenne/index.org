#+SETUPFILE: ../static/org-html-themes/setup/theme-readtheorg.setup
#+OPTIONS: html-postamble:nil
#+OPTIONS: toc:nil
 # +OPTIONS: toc:2
#+PROPERTY: header-args :results raw :exports results
 # The previous header seem to be required for code block results to be published..
 # header in code blocks has no effect..



#+NAME: ls
#+BEGIN_SRC sh
echo "#+TITLE: $(pwd|cut -d'/' -f4-)\n"
for name in $(ls -d */)
do
echo "* [[file:./$name/index.org][$name]]"
done

for name in $(ls |grep .org |grep -v "~" |grep -v "index.org" | cut -d"." -f1)
do
echo "* [[./$name.org][$name]] \n"
done

echo "[[../index.org][<-précédent]] \n"
#+END_SRC

#+RESULTS: ls




















