 # +SETUPFILE: ../../static/org-html-themes/setup/theme-readtheorg.setup
#+AUTHOR: Robert Biloute
#+date:
#+TITLE: Simulation électromagnétique avec meep
#+index: simulation
#+index: simulation!meep 



* Annexes

** Réflexion sur un miroir parabolique


[[file:../../static/ez_2D_focus.gif]]

Code:


#+INCLUDE: ../../static/code/2D_focus.ctl src scheme
