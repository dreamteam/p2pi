;;;; -*- Mode: scheme; Base: 10; coding: latin-1; -*-

;; * Parameters

(define-param sx 1) ; size of cell in X direction
(define-param sy 1) ; size of cell in Y direction
(define-param n 1) ; index of main volume
(define-param eps 1000) ; epsilon of absorbing plane with holes
(define-param r 5) ; radius of holes
(define-param zb (- 0 0)) ; z position of absorbing plane with holes
(define-param fcen 0.5) ; pulse center frequency
(define-param df 0.5)  ; pulse width (in frequency)

;; source position
(define-param xs 0)
(define-param ys 13)
(define-param zs 0)
(define-param dpml 5)

;; * Constants

(define sxy 100) ; cell size

;; * Init exec

(set! geometry-lattice 
      (make lattice (size sxy sxy no-size)))
(set! pml-layers 
      (list (make pml (thickness dpml))))

(set! geometry 
      (list
       (make cone 
	 (axis 0 1.3 1) 
	 (center 0 (- (/ sxy 4) 0) 40) 
	 (radius (+ (/ sxy 2) 5)) 
	 (radius2 5) 
	 (height (/ (* 2 sxy) 3)) 
	 (material metal))
       (make cone 
	 (axis 0 1.3 1) 
	 (center 0 (- (/ sxy 4) 0) 40) 
	 (radius (/ sxy 2)) 
	 (height (/ (* 2 sxy) 3)) 
	 (material air))))		

(set-param! resolution 4)

(set! sources 
      (list
       (make source
	 (src (make gaussian-src (frequency fcen) (fwidth df)))		 
	 (component Ez) (center xs ys) (size 0 0 0))))

;; * Exec

(run-until 0 output-epsilon)    
(run-until (/ 10 fcen) (at-every (/ 1 fcen 10) output-tot-pwr))
