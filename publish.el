(package-initialize)
;(require 'org)
(require 'ox)
(require 'ox-publish)
(require 'ox-latex)
(require 'ox-html)

;(add-to-list 'load-path (expand-file-name "/builds/dreamteam/p2pi/static/elisp" (file-name-directory load-file-name)))
;!(load "ox-rss.el")

;; attempt to replace ox-html.el so that TOC links are real url links
;; dans ox-html.el, il faut modifier la fonction org-html--format-toc-headline
;; (must also set :CUSTOM_ID property )
;; diff: (juste un "#" en moins)
;; *** my-ox-html.el       2018-02-01 12:50:14.059190764 +0100
;; --- /usr/share/emacs/site-lisp/org-mode/ox-html.el      2016-12-28 09:37:47.000000000 +0100
;; ***************
;; *** 2286,2290 ****
;; (tags (and (eq (plist-get info :with-tags) t)
;; 	   (org-export-get-tags headline info))))
;; !     (format "<a href=\"%s\">%s</a>"
;; ;; Label.
;; (or (org-element-property :CUSTOM_ID headline)
;;     --- 2286,2290 ----
;;     (tags (and (eq (plist-get info :with-tags) t)
;; 	       (org-export-get-tags headline info))))
;; !     (format "<a href=\"#%s\">%s</a>"
;; 	      ;; Label.
;; 	      (or (org-element-property :CUSTOM_ID headline)

;(add-to-list 'load-path (expand-file-name "/builds/dreamteam/p2pi/" (file-name-directory load-file-name)))
;(load "my-ox-html.el")
;(load "my-ox.el")
					;(load "my-org-compat.el")

;; (defun org-define-error (name message)
;;   "Define NAME as a new error signal.
;; MESSAGE is a string that will be output to the echo area if such
;; an error is signaled without being caught by a `condition-case'.
;; Implements `define-error' for older emacsen."
;;   (if (fboundp 'define-error) (define-error name message)
;;     (put name 'error-conditions
;; 	 (copy-sequence (cons name (get 'error 'error-conditions))))))


(setq org-image-actual-width nil)

;; tells org to not care about timestamps: publish whatever the file
;; timestamp is
;(setq org-publish-use-timestamps-flag nil)

(setq user-full-name "Johnny B. Goode")

(setq org-html-postamble-format 
      '(("en"
	 "<p class=\"author\">Author: %a </p><p class=\"date\">Date: %d</p><p class=\"creator\">Propulsé par: %c</p>")))

(setq org-startup-with-inline-images t)

(setq extra-head-for-thumbnail
      (concat
       "<meta property='og:title' content='test thumbnail'/>\n"
;       "<meta property='og:image' content='http://dreamteam.frama.io/p2pi/static/fringe_detection_efficiency_vs_df.png'/>\n"
       "<meta property='og:description' content='blablabla'/>\n"
       "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />"
       "<link rel='stylesheet' type='text/css' href='https://www.pirilampo.org/styles/readtheorg/css/htmlize.css'/>"
       "<link rel='stylesheet' type='text/css' href='https://www.pirilampo.org/styles/readtheorg/css/readtheorg.css'/>"
       "<script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>"
       "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js'></script>"
       "<script type='text/javascript' src='https://www.pirilampo.org/styles/lib/js/jquery.stickytableheaders.min.js'></script>"
       "<script type='text/javascript' src='https://www.pirilampo.org/styles/readtheorg/js/readtheorg.js'></script>"	      
       ))

;; no confirmation for evaluating code blocks export of auto sitemap
;; for subdirs)
(setq org-confirm-babel-evaluate nil)
(setq org-export-babel-evaluate t)
(setq org-export-use-babel t)

;; YES FInally code blocks are evaluated when publishing with this:
(org-babel-do-load-languages
 'org-babel-load-languages
 '((sh . t)))

;; try hacking publish func
;; (defun org-publish-org-to (backend filename extension plist &optional pub-dir)
;;   "Publish an Org file to a specified back-end.

;; BACKEND is a symbol representing the back-end used for
;; transcoding.  FILENAME is the filename of the Org file to be
;; published.  EXTENSION is the extension used for the output
;; string, with the leading dot.  PLIST is the property list for the
;; given project.

;; Optional argument PUB-DIR, when non-nil is the publishing
;; directory.

;; Return output file name."
;;   (unless (or (not pub-dir) (file-exists-p pub-dir)) (make-directory pub-dir t))
;;   ;; Check if a buffer visiting FILENAME is already open.
;;   (let* ((org-inhibit-startup t)
;; 	 (visiting (find-buffer-visiting filename))
;; 	 (work-buffer (or visiting (find-file-noselect filename))))
;;     (unwind-protect
;; 	(with-current-buffer work-buffer
;; 	  (let ((output (org-export-output-file-name extension nil pub-dir)))
;; 	    (org-export-to-file backend output
;; 	      ;;;;;;;; CHANGE HERE ;;;;;;;;;;;;
;;   	      ;nil nil nil (plist-get plist :body-only)
;; 	      t t t (plist-get plist :body-only)
;; 	      ;; Add `org-publish--store-crossrefs' and
;; 	      ;; `org-publish-collect-index' to final output filters.
;; 	      ;; The latter isn't dependent on `:makeindex', since we
;; 	      ;; want to keep it up-to-date in cache anyway.
;; 	      (org-combine-plists
;; 	       plist
;; 	       `(:crossrefs
;; 		 ,(org-publish-cache-get-file-property
;; 		   (expand-file-name filename) :crossrefs nil t)
;; 		 :filter-final-output
;; 		 (org-publish--store-crossrefs
;; 		  org-publish-collect-index
;; 		  ,@(plist-get plist :filter-final-output)))))))
;;       ;; Remove opened buffer in the process.
;;       (unless visiting (kill-buffer work-buffer)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; custom site map function;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun my-org-publish-org-sitemap (project &optional sitemap-filename)
  "Create a sitemap of pages in set defined by PROJECT.
Optionally set the filename of the sitemap with SITEMAP-FILENAME.
Default for SITEMAP-FILENAME is `sitemap.org'."
  (let* ((project-plist (cdr project))
	 (dir (file-name-as-directory
	       (plist-get project-plist :base-directory)))
	 (localdir (file-name-directory dir))
	 (indent-str (make-string 1 ?* ))
	 (exclude-regexp (plist-get project-plist :exclude))
	 (files (nreverse
		 (org-publish-get-base-files project exclude-regexp)))
	 (sitemap-filename (concat dir (or sitemap-filename "sitemap.org")))
	 (sitemap-title (or (plist-get project-plist :sitemap-title)
			  (concat "Sitemap for project " (car project))))
	 (sitemap-style (or (plist-get project-plist :sitemap-style)
			    'tree))
	 (sitemap-sans-extension
	  (plist-get project-plist :sitemap-sans-extension))
	 (visiting (find-buffer-visiting sitemap-filename))
	 file sitemap-buffer)
    (with-current-buffer
	(let ((org-inhibit-startup t))
	  (setq sitemap-buffer
		(or visiting (find-file sitemap-filename))))
      (erase-buffer)
      (insert (concat "#+OPTIONS: H:1 \n"))
      (insert (concat "#+TITLE: " sitemap-title "\n\n"))
      (while (setq file (pop files))
	(let ((link (file-relative-name file dir))
	      (oldlocal localdir))
	  (when sitemap-sans-extension
	    (setq link (file-name-sans-extension link)))
	  ;; sitemap shouldn't list itself
	  ;; (unless (equal (file-truename sitemap-filename)
	  ;; 		 (file-truename file))
  	  (unless (or (equal "index.org"
			     (file-name-nondirectory file))
		      (equal (file-truename sitemap-filename)
			 (file-truename file)))
	    (if (eq sitemap-style 'list)
		(message "Generating list-style sitemap for %s" sitemap-title)
	      (message "Generating tree-style sitemap for %s" sitemap-title)
	      (setq localdir (concat (file-name-as-directory dir)
				     (file-name-directory link)))
	      (unless (or (string=
			   (file-name-as-directory localdir)
			   "static/")
		       (string= localdir oldlocal))
		(if (string= localdir dir)
		    (setq indent-str (make-string 1 ?* ))
		  (let ((subdirs
			 (split-string
			  (directory-file-name
			   (file-name-directory
			    (file-relative-name localdir dir))) "/"))
			(subdir "")
			(old-subdirs (split-string
				      (file-relative-name oldlocal dir) "/")))
		    (setq indent-str (make-string 1 ?* ))
		    (while (string= (car old-subdirs) (car subdirs))
		      (setq indent-str (concat indent-str (make-string 1 ?* )))
		      (pop old-subdirs)
		      (pop subdirs))
		    (dolist (d subdirs)
		      (setq subdir (concat subdir d "/"))
		      ;(insert (concat indent-str " " subdir "\n"))
		      (insert (concat indent-str " [[file:" subdir "/index.org][" subdir "]]\n"))
		      ;; (insert ":PROPERTIES:\n")
		      ;; (insert (concat (concat ":CUSTOM_ID: " link) "\n"))
		      ;; (insert ":END:\n")
		      (setq indent-str (make-string
					(+ (length indent-str) 1) ?* )))))))
	    ;; This is common to 'flat and 'tree
	    (let ((entry
		   (org-publish-format-file-entry
		    org-publish-sitemap-file-entry-format file project-plist))
		  (regexp "\\(.*\\)\\[\\([^][]+\\)\\]\\(.*\\)"))
	      (cond ((string-match-p regexp entry)
		     (string-match regexp entry)

		     (insert (concat indent-str "\t " (match-string 1 entry)
				     " [[file:" link "]["
				     (match-string 2 entry)
				     "]]" (match-string 3 entry) "\n")))
		    (t
		     (insert (concat indent-str " [[file:" link "]["
				     entry
				     "]]\n"))))))))
      (save-buffer))
    (or visiting (kill-buffer sitemap-buffer))))

(defun my-force-generate-index ()
  (org-publish-index-generate-theindex (car org-publish-project-alist) "."))

(setq org-publish-project-alist
      `(("blog_p2pi"
	 ;:html-format-headline-function org-html-format-headline-default-function
	 :auto-sitemap t
	 :exclude "static"
	 :sitemap-function my-org-publish-org-sitemap
	 :sitemap-title "p2pi"
	 :sitemap-sort-files chronologically
	 :with-author "Jonny B. Goode"
	 :makeindex t
	 :recursive t
	 :with-latex t
	 :with-tags t
					;:auto-preamble t
	 :auto-postamble t ; not sure it even exists..
	 :html-postamble t
	 :html-head "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />"
	 :html-head "<link rel='stylesheet' type='text/css' href='https://www.pirilampo.org/styles/readtheorg/css/htmlize.css'/>"
	 :html-head "<link rel='stylesheet' type='text/css' href='https://www.pirilampo.org/styles/readtheorg/css/readtheorg.css'/>"
	 :html-head "<script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>"
	 :html-head "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js'></script>"
	 :html-head "<script type='text/javascript' src='https://www.pirilampo.org/styles/lib/js/jquery.stickytableheaders.min.js'></script>"
	 :html-head "<script type='text/javascript' src='https://www.pirilampo.org/styles/readtheorg/js/readtheorg.js'></script>"

	 :html-head-extra ,extra-head-for-thumbnail ;"<meta property='og:image' content='http://dreamteam.frama.io/p2pi/static/fringe_detection_efficiency_vs_df.png'/>" 
	 :html-link-home "http://dreamteam.frama.io/p2pi"

	 ;:exports results
	 ;:eval export
					;:html-inline-images t
	 :with-timestamps 100
	 
	 :with-drawers t
	 :base-directory "."
	 :base-extension "org"
	 :preparation-function my-force-generate-index ;delete-files-in-dir
	 ;:completion-function publish-p2pi-static
	 :publishing-directory "./public" ;"~/data/perso/prog/PhasedDongles/tmp/" ;
	 :publishing-function org-html-publish-to-html
	 :section-numbers nil
	 :with-toc t
	 :html-inline-images t
	 ;:latex-class article
	 :html-mathjax-options nil
	 ;; :html-head ,nico-website-html-head
	 ;; :html-preamble ,nico-website-html-preamble
	 ;; :html-postamble ,nico-website-html-postamble
	 )

	("p2pi-static"
	 :with-author "Jonny B. Goode"
	 :base-directory "./static"
	 :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
	 :publishing-directory "./public/static"
	 :recursive t
	 :publishing-function org-publish-attachment)

	;; ("rss"
	;;  :base-directory "."
	;;  :base-extension "org"
	;;  :publishing-directory "./public"
	;;  :publishing-function (org-rss-publish-to-rss)
	;;  :html-link-home "http://dreamteam.frama.io/p2pi"
        ;;  :html-link-use-abs-url t)
	
	("p2pi" :components ("blog_p2pi" "p2pi-static"))
	
	;; ("myprojectorg"
	;;  :base-directory "/home/censier/data/perso/DaHouse/"
	;;  :publishing-directory "/rsync:root@emplug:/home/robert/data/www/DaHouse/"
	;;  :publishing-function org-html-publish-to-html
	;;  :auto-preamble t
	;;  )
	))

(version)
